import java.util.concurrent.TimeUnit

import utils.ServerCommunication
import utils.modbusclient.ModbusClient
import utils.model.SignalProcessingBlocks
import utils.model.controller.{Controller, PIDController}
import utils.model.parameters.{Outputs, Parameters}
import utils.model.plants.{BuildingModel, RadiatorModel, Constants}
import utils.solver.Euler

import scala.annotation.tailrec

/**
  * Created by bszmit on 11/14/15.
  */
object Simulation extends App {
  // Initial conditions
  val Tpco_initial: Double = 15 + 273
  val Fcob_initial: Double = 40.0 / 3600.0
  val Tzco_initial: Double = 40.0 + 273
  val Tr_initial: Double = 10 + 273
  val To_initial: Double = 10 + 273
  val setPoint: Double = 15 + 273 //setpoint

  // Controller parameters
  val P = 2.0
  val I = 0.0
  val D = 0.0
  val sampleTime = Euler.h

  val time: Double = 0

  val pid = new PIDController(P, I, D, sampleTime)

  @tailrec def loop(iterations: Int, controller: Controller, simulationTime_seconds: Double, setPoint: Double,
                    time: Double, previousParams: Parameters) : Parameters = {
    val params: Parameters = Loop.iterationFromParams(pid, simulationTime_seconds, setPoint, time, previousParams)
    println(params)
    if (iterations > 0) {
      return loop(iterations - 1, controller, simulationTime_seconds, setPoint, time, params)
    }

    else
      return params
  }

  val  serverCommunication = new ServerCommunication(ModbusClient("10.0.3.1", 1234))
  def loop2(old_min: Int, iterations: Int, old_params: Parameters): Unit = {
    if (iterations > 0) {
      val inputs = serverCommunication.updateInputs()
      print(inputs)
      print(old_params)
      val old_min = inputs.Min
      val params = new Parameters(inputs.To, inputs.Tzco, inputs.Day, inputs.Hour, inputs.Min, old_params.Tpco, old_params.Fcob, old_params.Tr)

      val new_params = loop(0, pid, 60, setPoint, time, params)

      var newTime = serverCommunication.updateInputs().Min
      while (newTime <= old_min) {
        TimeUnit.SECONDS.sleep(1)
        println("czekam")
        newTime = serverCommunication.updateInputs().Min
      }
      println("dalej")
      serverCommunication.writeOutputs(new_params)

      loop2(newTime, iterations-1, new_params)
    }

  }

  val params: Parameters = new Parameters(To_initial, Tzco_initial, 0, 0, 0, Tpco_initial, Fcob_initial, Tr_initial)
  val start = System.currentTimeMillis()
  loop2(0, 100, params)
//  loop(10, pid, 60, setPoint, time, params)
  val stop = System.currentTimeMillis()
  println((stop- start).toDouble / 1000 / 10)

}

object Loop {
  val saturation = SignalProcessingBlocks.saturationFactory(0.0, 1.0)

  def iterationFromParams(controller: Controller, simulationTime_seconds: Double, setPoint: Double,
                          time: Double, params: Parameters): Parameters = {
    val Tpco = params.Tpco
    val Tr = params.Tr
    val Tzco = params.Tzco
    val To = params.To
    iteration(controller, simulationTime_seconds, setPoint, time, Tpco, Tr, Tzco, To)
  }

  def iteration(controller: Controller, simulationTime_seconds: Double, setPoint: Double,
                _time: Double, _Tpco: Double, _Tr: Double, _Tzco: Double, _To: Double): Parameters = {
    val error: Double = setPoint - _Tr
    val fcob: Double = saturation(controller.compute(error)) * Constants.Fcob_max

    val (time, tpco): (Double, Double) = Euler.solve(_time, _Tpco) {
                                                                     RadiatorModel(_Tpco, fcob, _Tzco, _Tr)
                                                                   }
    val (_, tr): (Double, Double) = Euler.solve(_time, _Tr) {
                                                              BuildingModel(_Tr, tpco, _To)
                                                            }
    if (time < simulationTime_seconds)
      iteration(controller, simulationTime_seconds, setPoint, _time + Euler.h, tpco, tr, _Tzco, _To)
    else
      new Parameters(_To, _Tzco, 0, 0, 0, tpco, fcob, tr)

  }
}
