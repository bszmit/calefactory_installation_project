import java.util.concurrent.TimeUnit

import utils.ServerCommunication
import utils.modbusclient.ModbusClient
import utils.model.parameters.{Outputs, Inputs}

/**
 * Created by bszmit on 11/2/15.
 */

object Testing extends App {
//  val modbusClient = ModbusClient("192.168.1.9", 20001)
  val modbusClient = ModbusClient("10.0.3.1", 1234)
  val simulation = new ServerCommunication(modbusClient)

//  while (true) {
    val a: Inputs = simulation.updateInputs()
    simulation.writeOutputs(Outputs(12.5, 13.3, 14))
    println(a)
//    a.about()
//    println(x,y,z)
//    simulation.writeOutputs(x, y, z)
//    simulation.writeOutputs(22, 23, 25)
    TimeUnit.SECONDS.sleep(1)
//  }


//  val modbusClient = ModbusClient("10.0.3.1", 1234)
//  modbusClient.about()
//  println(modbusClient.readInputRegisters(0 to 24: _*))
//  modbusClient.writeRegisters(11, 12, 13)(14, 15, 16)
}
