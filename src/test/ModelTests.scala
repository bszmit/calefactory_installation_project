package test

import java.io.{BufferedWriter, File, PrintWriter}

import utils.csvutils.CSVWriter
import utils.model.controller.PIDController
import utils.model.parameters.Parameters
import utils.model.plants.{RadiatorModel, BuildingModel}
import utils.solver.Euler

import scala.annotation.tailrec

/**
  * Created by bszmit on 11/13/15.
  */
object ModelTests {


  def radiatorModel(iterations: Int) {
    val writer = new PrintWriter(new File("data_files/radiator_tests.txt"))
    try {
      // Initial conditions
      val Tpco: Double = 0
      val Fcob: Double = 40.0 / 3600.0
      val Tzco: Double = 40 + 273
      val Tr: Double = 10 + 273
      val time: Double = 0

      loop(time, Tpco)

      def loop(time_ : Double, Tpco_ : Double) {
        val (time, tpco): (Double, Double) = Euler.solve(time_, Tpco_) {
                                                                         RadiatorModel(Tpco_, Fcob, Tzco, Tr)
                                                                       }
        writer.write(time.toString + ',' + tpco.toString + '\n')
        if (time < iterations)
          loop(time, tpco)
      }

    } catch {
      case e: Throwable => println(e.getMessage)
    } finally {
      writer.close()
    }
  }

  def buildingModel(iterations: Int) {
    val writer = new PrintWriter(new File("data_files/building_tests.txt"))
    try {
      // Initial conditions
      val Tpco: Double = 15 + 273
      val To: Double = 10 + 273
      val Tr: Double = 10 + 273
      val time: Double = 0

      loop(time, Tr)

      def loop(time_ : Double, Tr_ : Double) {
        val (time, tr): (Double, Double) = Euler.solve(time_, Tr_) {
                                                                     BuildingModel(Tr_, Tpco, To)
                                                                   }
        writer.write(time.toString + ',' + tr.toString + '\n')
        if (time < iterations)
          loop(time, tr)
      }

    } catch {
      case e: Throwable => println(e.getMessage)
    } finally {
      writer.close()
    }
  }

  def radiator_building_openLoop(simulation_time_seconds: Int) {

    val writer = new BufferedWriter(new PrintWriter("data_files/radiator_building_openloop_tests.txt"))
    val csvwriter_file = new CSVWriter(writer, new Parameters())

    //    val fifo = new PrintWriter("../myfifo")
    //    val csvwriter_fifo = new CSVWriter(fifo, new Parameters())

    try {
      // Initial conditions

      val Tpco: Double = 0
      val Fcob: Double = 40.0 / 3600.0
      val Tzco: Double = 40 + 273
      val Tr: Double = 10 + 273
      val To: Double = 10 + 273
      val time: Double = 0


      def loop(_time: Double, _Tpco: Double, _Tr: Double) {
        val (time, tpco): (Double, Double) = Euler.solve(_time, _Tpco) {
                                                                         RadiatorModel(_Tpco, Fcob, Tzco, _Tr)
                                                                       }
        val (_, tr): (Double, Double) = Euler.solve(_time, _Tr) {
                                                                  BuildingModel(_Tr, tpco, To)
                                                                }
        val params = new Parameters(To, Tzco, 0, 0, (time * 100).toInt, _Tpco, Fcob, tr)
        //        val params = new Parameters()
        //        csvwriter_file.write(new Parameters(0, 1, 2, 3, 4, 5, 6, 7))
        csvwriter_file.write(params)
        //        csvwriter_fifo.write(params)

        if (time < simulation_time_seconds)
          loop(time, tpco, tr)

      }
      loop(time, Tpco, Tr)

    } catch {
      case e: Throwable => println("wyjatek"); println(e.getMessage)
    } finally {
      writer.close()
      println("writer.close")
      //      csvwriter_fifo.close()
      println("fifo.close")
    }
  }

  def PID_test(simulation_time_seconds: Int) {
    val writer = new BufferedWriter(new PrintWriter("data_files/PID_tests.txt"))
    val csvwriter_file = new CSVWriter(writer, new Parameters())

        val fifo = new PrintWriter("data_files/myfifo")
        val csvwriter_fifo = new CSVWriter(fifo, new Parameters())

    try {
      // Initial conditions
      val pid = new PIDController(0.5, 0.5, 0.0005, 0.005)


      val time: Double = 0
      val setPoint: Double = 1
      val u: Double = 0
      def loop(_time: Double, last_u: Double) {
        val error = setPoint - last_u
        val _u = pid.compute(error)

        csvwriter_file.write(new Parameters(_u, 0, 0, 0, (_time*100).toInt, 0, 0, 0))
        csvwriter_fifo.write(new Parameters(_u, 0, 0, 0, (_time*100).toInt, 0, 0, 0))
        if (_time < simulation_time_seconds)
          loop(_time + 0.005, _u)
      }
      loop(time, u)

    } catch {
      case e: Throwable => println("wyjatek"); println(e.getMessage)
    } finally {
      csvwriter_file.close()
      csvwriter_fifo.close()
      println("writer.close")
      //      csvwriter_fifo.close()
      println("fifo.close")
    }
  }


}

object Tests extends App {
//  ModelTests.radiatorModel(150)
//  ModelTests.buildingModel(150)
//  val date: java.util.Date = new java.util.Date();

//  ModelTests.radiator_building_openLoop(60)
  ModelTests.PID_test(10)
  //  ModelTests.radiator_building_openLoop((60.0 / Euler.h).toInt)
  println("koniec")

  //    val stop: Long = System.currentTimeMillis
  //    stop-start

}




