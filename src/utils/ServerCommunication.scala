package utils

import utils.modbusclient.ModbusClient
import utils.model.parameters.{Inputs, Outputs}

/**
  * Created by bszmit on 11/2/15.
  */

object ServerCommunication {
  private val multiplicator = 100
  private val encodeForServer = (x: Double) => (x * multiplicator).toInt : Int
  private val decodeFromServer = (x: Int) => x.toDouble / multiplicator : Double
}

class ServerCommunication(private val modbusClient: ModbusClient) {


  def updateInputs(): Inputs = {

    val to :: tzco :: day :: hour :: min :: Nil = modbusClient.readInputRegisters(Inputs.Address.To,
                                                                                  Inputs.Address.Tzco,
                                                                                  Inputs.Address.day,
                                                                                  Inputs.Address.hour,
                                                                                  Inputs.Address.min)
    val to_double = ServerCommunication.decodeFromServer(to)
    val tzco_double = ServerCommunication.decodeFromServer(tzco)

    Inputs(_To = to_double, _Tzco = tzco_double, _day = day, _hour = hour, _min = min)
  }

  def writeOutputs(outputs: Outputs) {
    val Fcob5 = ServerCommunication.encodeForServer(outputs.Fcob)
    val Tpco5 = ServerCommunication.encodeForServer(outputs.Tpco)
    val Tr5 = ServerCommunication.encodeForServer(outputs.Tr)
    modbusClient.writeRegisters(Outputs.Address.Fcob5,
                                Outputs.Address.Tpco5,
                                Outputs.Address.Tr5)(Fcob5, Tpco5, Tr5)
  }
}




