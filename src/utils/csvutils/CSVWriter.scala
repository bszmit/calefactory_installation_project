package utils.csvutils

import java.io.Writer

/**
  * Created by bszmit on 11/14/15.
  */
class CSVWriter(private val file: Writer, val obj: CSVable) {
  file.write(obj.CSVheader + '\n')
  file.flush()

  def write(obj: CSVable): Unit =  { file.write(obj.toCSV + '\n'); file.flush()}

  def close(): Unit = file.close()
}
