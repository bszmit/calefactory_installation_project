package utils.modbusclient

import java.net.InetAddress

import net.wimpi.modbus.io.ModbusTCPTransaction
import net.wimpi.modbus.msg._
import net.wimpi.modbus.net.TCPMasterConnection
import net.wimpi.modbus.procimg.SimpleRegister

/**
*  Created by bszmit on 11/1/15.
*/


/**
 * Factory object for ModbusClient.
 * Produces instances responsible
 * for communication with Modbus server via TCP/IP.
 */
object ModbusClient {
  /**
   * @param ip The IP address of Modbus server.
   * @param port The port of Modbus server.
   */
  def apply(ip: String, port: Int): ModbusClient = {
    val inetAddress: InetAddress = InetAddress.getByName(ip)

    new ModbusClient(inetAddress, port)
  }

  private def getTCPTransaction(connection: TCPMasterConnection)
  : ModbusTCPTransaction = new ModbusTCPTransaction(connection)
}


/**
 * Class for communication with Modbus server via TCP/IP.
 * @param addr Modbus server IP address
 * @param port Modbus server port
 */
class ModbusClient private(addr: InetAddress, port: Int) {
  //private constructor
  val connection: TCPMasterConnection = new TCPMasterConnection(addr)
  connection.setPort(port)
  connection.connect()
  //closure
  val getTCPTransaction = () => ModbusClient.getTCPTransaction(connection): ModbusTCPTransaction

  def about() {
    println(addr)
    println(port)
  }

  /**
   * Returns <code>List</code> of input registers
   * values from assigned Modbus server.
   * @param registersAddresses <code>List</code> of registers addresses
   * @return <code>List</code> of register values
   */
  def readInputRegisters(registersAddresses: Int*): List[Int] = {

    //pattern matching
    registersAddresses.toList match {
      case registerAddress :: tailRegistersAddresses =>
        val request = new ReadMultipleRegistersRequest(registerAddress, 1)
        val transaction = getTCPTransaction()

        transaction.setRequest(request)
        transaction.execute()

        val response: ReadMultipleRegistersResponse= transaction.getResponse.asInstanceOf[ReadMultipleRegistersResponse]

        response.getRegisterValue(0) :: readInputRegisters(tailRegistersAddresses: _*)

      case _ => Nil
    }
  }


  /**
   * Writes values to registers.
   * Each register is written with value respectively to order in lists.
   * Curried function.
   * @param registersAddresses Addresses of registers to be written.
   * @param values Values to be written to registers.
   */
  def writeRegisters(registersAddresses: Int*)(values: Int*) {
    require(registersAddresses.length == values.length)

    registersAddresses.toList match {
      case registerAddress :: tailRegistersAddresses =>
        val value :: tailValues = values.toList
        val request = new WriteSingleRegisterRequest(registerAddress, new SimpleRegister(value))
        val transaction = getTCPTransaction()
        transaction.setRequest(request)
        transaction.execute()

        writeRegisters(tailRegistersAddresses: _*)(tailValues: _*)
      case _ =>
    }

  }
}

