package utils.model

/**
  * Created by bszmit on 11/15/15.
  */
object SignalProcessingBlocks {
  /**
    * Generates saturate function literal with given bounds
    * @return saturate function literal with fixed lower and upper bounds
    */
  def saturationFactory(lowerBound: Double, upperBound: Double) : Double => Double = {
    (signal: Double) =>
      if (signal < lowerBound) lowerBound
      else if (signal > upperBound) upperBound
      else signal
  }
}
