package utils.model.controller

/**
  * Created by bszmit on 11/14/15.
  */
trait Controller {
  /**
    * Compute steering
    * @param error difference between setpoint value and current value
    * @return
    */
  def compute(error: Double) : Double

}
