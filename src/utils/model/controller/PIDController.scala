package utils.model.controller

/**
  * Created by bszmit on 11/14/15.
  */
class PIDController(_P: Double,
                    _I: Double,
                    _D: Double,
                    private val sampleTime: Double) extends Controller {

  private val P: Double = _P
  private val I: Double = _I * sampleTime
  private val D: Double = _D / sampleTime

  var integral: Double = 0 // accumulating error for I part
  var lastError: Double = 0 // remembering last error for D part

  /**
    * Computes positional version of PID algorithm
    * @param error difference between setpoint value and current value
    * @return steering
    */
  private def computePositional(error: Double): Double = {
    integral += error

    val P_part: Double = P * error
    val I_part: Double = I * integral
    val D_part: Double = D * (error - lastError)

    lastError = error
    P_part + I_part + D_part
  }

  /**
    * Compute steering
    * @param error difference between setpoint value and current value
    * @return steering
    */
  override def compute(error: Double): Double = computePositional(error)

}
