package utils.model.parameters

import utils.csvutils.CSVable

/**
  * Created by bszmit on 11/14/15.
  */
object Inputs {
  def apply(_To: Double, _Tzco: Double, _day: Int, _hour: Int, _min: Int): Inputs = {
    new Inputs {
      override val To: Double = _To
      override val Tzco: Double = _Tzco
      override val Day: Int = _day
      override val Hour: Int = _hour
      override val Min: Int = _min
    }

  }

  object Address {
    val To = 19
    val Tzco = 23
    val day = 30
    val hour = 29
    val min = 28
  }

}


trait Inputs extends CSVable {
  val To: Double
  val Tzco: Double
  val Day: Int
  val Hour: Int
  val Min: Int

  override def CSVheader:String = "To, Tzco, Day, Hour, Min"
  override def toCSV: String = f"$To%f, $Tzco%f, $Day%d, $Hour%d, $Min%d"

  override def toString = {
    "Inputs:\n" +
      f"To: $To%f\n" +
      f"Tzco: $Tzco%f\n" +
      f"day: $Day%d\n" +
      f"hour: $Hour%d\n" +
      f"min: $Min%d\n"
  }
}