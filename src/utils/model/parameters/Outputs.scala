package utils.model.parameters

import utils.csvutils.CSVable

/**
  * Created by bszmit on 11/14/15.
  */

object Outputs {
  def apply(_Fcob: Double, _Tpco: Double, _Tr: Double): Outputs = {
    new Outputs {
      override val Tpco: Double = _Tpco
      override val Fcob: Double = _Fcob
      override val Tr: Double = _Tr
    }
  }

  object Address {
    val Fcob5 = 13
    val Tpco5 = 14
    val Tr5 = 15
  }

}

trait Outputs extends CSVable {
  val Fcob: Double
  val Tpco: Double
  val Tr: Double

  override def CSVheader: String = "Fcob, Tpco, Tr"

  override def toCSV: String = f"$Fcob%f, $Tpco%f, $Tr%f"

  override def toString = {
    "Outputs:\n" +
      f"$Fcob%f\n" +
      f"$Tpco%f\n" +
      f"$Tr%f\n"
  }

}
