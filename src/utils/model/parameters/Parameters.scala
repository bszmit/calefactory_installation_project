package utils.model.parameters

import utils.csvutils.CSVable

/**
  * Created by bszmit on 11/14/15.
  */
class Parameters(_To: Double,
                 _Tzco: Double,
                 _day: Int,
                 _hour: Int,
                 _min: Int,
                 _Tpco: Double,
                 _Fcob: Double,
                 _Tr: Double) extends Inputs with Outputs with CSVable {
  /**
    * Only inputs
    */
  def this(_To: Double, _Tzco: Double, _day: Int, _hour: Int, _min: Int) = this(_To, _Tzco, _day, _hour, _min, 0, 0, 0)

  /**
    * Only outputs
    */
  def this(_Tpco: Double, _Fcob: Double, _Tr: Double) = this(0, 0, 0, 0, 0, _Tpco, _Fcob, _Tr)

  /**
    * Empty instance
    */
  def this() = this(0, 0, 0, 0, 0, 0, 0, 0)

  override val To: Double = _To
  override val Tzco: Double = _Tzco
  override val Day: Int = _day
  override val Hour: Int = _hour
  override val Min: Int = _min
  override val Tpco: Double = _Tpco
  override val Fcob: Double = _Fcob
  override val Tr: Double = _Tr

  override def CSVheader:String = "To, Tzco, Day, Hour, Min, Fcob, Tpco, Tr"
  override def toCSV: String = f"$To%f, $Tzco%f, $Day%d, $Hour%d, $Min%d, " + f"$Fcob%f, $Tpco%f, $Tr%f"
}


