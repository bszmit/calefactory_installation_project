package utils.model.plants

/**
  * Created by bszmit on 11/13/15.
  */
object BuildingModel {
  def apply(Tr: Double, Tpco: Double, To: Double): Double = {
    // import all constants from Constants object
    import Constants._
    (kh * (Tpco - Tr) - kext * (Tr - To)) / (mb * cb)
  }
}
