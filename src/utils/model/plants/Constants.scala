package utils.model.plants

/**
  * Created by bszmit on 11/13/15.
  */


/**
  * Stores physical constants.
  */
object Constants {

  val xsi = 1000  // kg/m^3 - density of water
  val cw = 4200 // J/kg/K - water specific heat

  val mh = 3000 // kg - radiator mass
  val ch = 2700 // J/kg/K - radiator specific heat
  val kh = 12000  // J/K/s - radiator <-> room heat transfer coefficient
  val Fcob_max: Double = 40.0 / 3600.0  // m^3 /s - max water consumption

  val mb = 20000  // kg - air mass in room
  val cb = 1000 // J/kg/K - air specific heat
  val kext = 15000  // J/K/s - air rate of heat loss
}
