package utils.model.plants

/**
  * Created by bszmit on 11/13/15.
  */
object RadiatorModel {
  def apply(Tpco: Double, Fcob: Double, Tzco: Double, Tr: Double): Double = {
    // import all constants from Constants object
    import Constants._
    (Fcob * xsi * cw * (Tzco - Tpco) - kh * (Tpco - Tr)) / (mh * ch)
  }
}

