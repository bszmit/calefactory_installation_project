package utils.solver

/**
  * Created by bszmit on 11/13/15.
  */

object Euler extends Solver {
  val h: Double = 0.05 // step

  override def solve(t: Double, y: Double)(op: Double): (Double, Double) = (t + h, y + h * op)
}
