package utils.solver

/**
  * Created by bszmit on 11/13/15.
  */
trait Solver {
  /**
    * Solves differential equation dy/dt = f(t, y)
    *
    * @param t independent variable
    * @param y differential variable
    * @param op equation dy/dt = op()
    * @return (t, y) tuple
    */
  def solve(t: Double, y: Double)(op: Double): (Double, Double)
}
